# Jupyter in Galaxy za analizo podatkov v odprti znanosti.

Ta repozitorij je namenjen spoznavanju Jupyter projekta in Galaxy platforme.
Tukaj najdete predstavitev, ki se predstavlja na predavanjih, primere zbirk nalog, pomoč pri reševanju nalog in nekatera navodila za namestitev.
V samem README-ju pa najdete še navedene pomembne povezave in povezave katere priporočamo za nadgradnjo znanja.

## Namestitev Jupyter Labs

#### Uporaba v brskalniku:

[Google Colab](https://colab.research.google.com/?utm_source=scs-index) <br/>
[JupyterLite](https://jupyter.org/try-jupyter/lab/)

#### Uporaba z Anaconda distribucijo:

[JupyterLab - Anaconda](https://www.anaconda.com/products/distribution)

## Uporaba Galaxy platform - useGalaxy:

[useGalaxy.org - glavni zastonjski strežnik](https://usegalaxy.org/) <br/>
[useGalaxy.eu - Evropski zastonjski strežnik](https://usegalaxy.eu/) <br/>
[live.useGalaxy.eu - za zagon interaktivnih orodij, kot so Jupyter, RStudio ali druge spletne aplikacije](https://live.usegalaxy.eu/) <br/>

## Koristne povezave za razumevanje Jupyter projekta:

[Project Jupyter | Home](https://jupyter.org/) <br/>
[The Jupyter Notebook dokumentacija](https://jupyter-notebook.readthedocs.io/en/stable/) <br/>
[The JupyterLab dokumentacija](https://jupyterlab.readthedocs.io/en/stable/) <br/>
[JupyterLab: The evolution of the Jupyter web interface](https://www.oreilly.com/content/jupyterlab-the-evolution-of-the-jupyter-web-interface/) <br/>
[Why Jupyter is data scientists’ computational notebook of choice](https://www.nature.com/articles/d41586-018-07196-1) <br/>
[Project Jupyter: A Computer Code that Transformed Science](https://crd.lbl.gov/news-and-publications/news/2021/project-jupyter-a-computer-code-that-transformed-science/) <br/>
[Jupyter Notebook for Open Science](https://reproducible-analysis-workshop.readthedocs.io/en/latest/4.Jupyter-Notebook.html) <br/>
[Using the Jupyter Notebook as a Tool for Open Science: An Empirical Study](https://ieeexplore.ieee.org/document/7991618) <br/>
[Ten Simple Rules for Reproducible Research in Jupyter Notebooks](https://arxiv.org/ftp/arxiv/papers/1810/1810.08055.pdf) <br/>
[Zero to JupyterHub with Kubernetes](https://zero-to-jupyterhub.readthedocs.io/en/latest/) <br/>

## Koristne povezave za razumevanje Galaxy platforme:

[Galaxy Community Hub - galaxyproject.org](https://galaxyproject.org/) <br/>
[GalaxyProject - YouTube channel](https://www.youtube.com/c/GalaxyProject) <br/>
[usegalaxy.eu - about](https://galaxyproject.eu/about.html) <br/>
[Jupyter and Galaxy: Easing entry barriers into complex data analyses for biomedical researchers](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5444614/) <br/>
[The Galaxy platform for accessible, reproducible and collaborative biomedical analyses](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6030816/) <br/>
[Ten Simple Rules for Setting up a Galaxy Instance as a Service](https://galaxyproject.org/admin/ten-simple-steps-galaxy-as-a-service/) <br/>
[Galaxy API](https://galaxyproject.org/develop/api/) <br/>
[Connecting Galaxy to a compute cluster](https://training.galaxyproject.org/training-material/topics/admin/tutorials/connect-to-compute-cluster/tutorial.html) <br/>

## Povezava do zbirke podatkov Iris.csv uporabljene na predstavitvi:

https://zenodo.org/record/1319069/files/iris.csv
